*** Settings ***
Library    ../../Library/customLibrary.py
Resource    ../../Resource/Common.robot
Library    Collections

*** Variables ***
${cart_btn}    //div[contains(@id, 'shopping_cart')]
${checkout_btn}    //button[@id='checkout']
${first_name}    //input[@id='first-name']
${last_name}    //input[@id='last-name']
${postal_code}    //input[@id='postal-code']
${continue_btn}    //input[@name='continue']
${finsh_btn}    //button[@id='finish']
${item_list}    (//div[@class='cart_list'])

*** Keywords ***
Count Item In List
    ${count}    get element count    ${item_list}
    skip if    ${count} == 0
    [Return]    ${count}
Click Checkout Button
    Run Keyword Until Success    click element    ${checkout_btn}
Enter First Name
    Run Keyword Until Success    input text    ${first_name}    sreyroth
Enter Last Name
    Run Keyword Until Success    input text    ${last_name}    on
Enter Postal Code
    Run Keyword Until Success    input text    ${postal_code}    855
Click Continue Button
    Run Keyword Until Success    click element    ${continue_btn}
Click Finish Button
    Run Keyword Until Success    click element    ${finsh_btn}