*** Settings ***
Library    SeleniumLibrary
Resource    ../Product/PageObject/Login.robot

*** Variables ***
${cart_btn}    //div[contains(@id, 'shopping_cart')]
${add_to_cart_btn}   //button[text()='Add to cart']

*** Keywords ***
Run Keyword Until Success
    [Arguments]    ${Keyword}    @{KW_Arguments}
    wait until keyword succeeds    10s    1s    ${Keyword}    @{KW_Arguments}
Add To Cart
    Run Keyword Until Success    click element    ${add_to_cart_btn}
Click Cart Button
    Run Keyword Until Success    click element    ${cart_btn}