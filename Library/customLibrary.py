import os
import platform

from dotenv import load_dotenv
import json, os, random

def override_env(key, value):
    load_dotenv(dotenv_path=os.getcwd()+"/.env")
    if value:
        os.environ[key] = value

def get_env(env, default=''):
    load_dotenv(dotenv_path=os.getcwd()+"/.env")
    env = os.getenv(env)
    if not env:
        return default
    return env

def generate_random_number(start, end):
    ranNum = random.randint(int(start), int(end))
    return ranNum

def Store_Data_as_Key_Value_to_Global_Test_Data(key, value):
    PWD = os.path.abspath(os.path.dirname(__file__))
    with open(f"{PWD}/../TestData/GlobalTestData.json", 'r') as f:
        data = json.loads(f.read())  # go to read data

    data[key] = value

    # go to write data
    with open(f"{PWD}/../TestData/GlobalTestData.json", 'w') as f:
        f.write(json.dumps(data, sort_keys=True, indent=4, separators=(',', ': ')))

